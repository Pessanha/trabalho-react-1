import React, { Component } from 'react'
import Header from '../../components/Header';

import { listNotes, saveNotes, delNote, updateNote } from '../../services/serviceNotes'

import './style.css'

export default class Notes extends Component {
  
  state = {
    notes: [],
    loading: true,
    loadingSave: false,
    text: '',
    search: ''
  }

  async componentDidMount () {
    this.setState({
      notes: await listNotes(),
      loading: false
    })
  }

  newNote = () => {
    this.setState({
      new: true
    })
  }

  setSearch = (event) => {
    this.setSearch({
      search: event.target.value
    })
  }

  editText = (event) => {
    this.setState({
      text: event.target.value
    })
  }

  saveNote = async () => {
    this.setState({
      loadingSave: true
    })
    await saveNotes(this.state.text)
    this.setState({
      notes: await listNotes(),
      loadingSave: false,
      new: false,
      text: ''
    })
  }

  deleteNote = async id => {
    await delNote(id)
    this.setState({
      notes: await listNotes()
    })
  }

  editNote = async note => {
    await updateNote(note)
    this.setState({
      notes: await listNotes()
    })
  }
  
  render() {
    return (
      <div>
        <Header/>
        <div className='content'>
          
          <div className='filters'>
            <div className='left'>
              <input type='search' placeholder='Procurar...' onChange={this.setSearch} />
            </div>
            <div className='right'>
              <button onClick={this.newNote}>Nova nota</button>
            </div>
          </div>

          <hr />

          <div className='list'>
            { this.state.loading && (
              <div className='item item-empty'>
                Carregando anotações...
              </div>
            ) }
            { !this.state.notes.length && !this.state.loading && (
              <div className='item item-empty'>
                Nenhuma nota criada até o momento!
              </div>
            ) }
            { this.state.new && (
              <div className='item'>
                <input onChange={this.editText} className='left' type='text' placeholder='Digite sua anotação' />
                <div className='right'>
                  { !this.state.loadingSave && (<button onClick={this.saveNote}>Salvar</button>) }
                  { this.state.loadingSave && (<span>Salvando...</span>) }
                </div>
              </div>
            )}
            { this.state.notes.map(note => !this.state.text.includes(note.text) && (
              <div className='item' key={note.id}>
                <input onChange={this.editText} className='left' type='text' defaultValue={note.text} placeholder='Digite sua anotação' />
                <div className='right'>
                  { !note.id && (<button onClick={this.saveNote}>Salvar</button>) }
                  { note.id && (<button onClick={this.deleteNote.bind(this, note.id)}>Excluir</button>) }
                  { note.id && (<button onClick={this.editNote.bind(this, note)}>Editar</button>) }
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    )
  }
}
