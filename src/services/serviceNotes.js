const result = [
    { text: 'Lavar o carro', id: new Date().getTime() + 100 },
    { text: 'Dar vacina na Alice', id: new Date().getTime() + 200 }
]

const listNotes = () => {

    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(result)
        }, 2000)
    })
}


const saveNotes = (text) => {

    return new Promise((resolve, reject) => {
        setTimeout(() => {
            result.unshift({
                text: text,
                id: new Date().getTime()
            })
            resolve()
        }, 2000)
    })
}

const delNote = id => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            result.splice(result.findIndex(e => e.id === id), 1)
            console.log(result)
            resolve()
        }, 2000)
    })
}

const updateNote = note => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            result.forEach(e => {
                if (note.id === e.id) {
                    e.text = note.text
                }
            })
            resolve()
        }, 2000)
    })
}


module.exports = {
    listNotes,
    saveNotes,
    delNote,
    updateNote
}